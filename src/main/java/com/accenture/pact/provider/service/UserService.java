package com.accenture.pact.provider.service;

import com.accenture.pact.provider.pojo.UserResponse;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	public UserResponse getUserDetails(String userId) {
		if ("12345".equalsIgnoreCase(userId)) {
			String jsonString = "{\"userId\":\"12345\",\"firstName\":\"SOURAV\",\"lastName\":\"DAS\",\"age\":32,\"nationality\":\"INDIAN\",\"email\":\"sourav.das@engineering.digital.dwp.gov.uk\"}";
			Gson gson = new Gson();
			return gson.fromJson(jsonString, UserResponse.class);
		} else {
			return new UserResponse();
		}
	}
}
