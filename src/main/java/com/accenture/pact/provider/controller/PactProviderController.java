package com.accenture.pact.provider.controller;

import com.accenture.pact.provider.pojo.UserResponse;
import com.accenture.pact.provider.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.pact.provider.pojo.DateResponse;
import com.accenture.pact.provider.service.DateService;

@RestController
@RequestMapping("/")
public class PactProviderController {

	@Autowired
	private DateService dateService;

	@Autowired
    private UserService userService;

    @GetMapping("/provider/validDate")
    public DateResponse getValidDate(@RequestParam(name = "date") String date) {
        return dateService.getValidDate(date);
    }

    @GetMapping("/provider/users")
    public UserResponse getUserData(@RequestParam(name = "userId") String userId) {
        return userService.getUserDetails(userId);
    }
}
