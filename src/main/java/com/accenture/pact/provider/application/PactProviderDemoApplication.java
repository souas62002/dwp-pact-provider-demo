package com.accenture.pact.provider.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages="com.accenture.pact.provider")
public class PactProviderDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(PactProviderDemoApplication.class, args);
    }
}
