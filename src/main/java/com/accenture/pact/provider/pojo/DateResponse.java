package com.accenture.pact.provider.pojo;

public class DateResponse {

	private String givenDate;
	private int year;
	private int month;
	private int day;
	private Boolean isValidDate = false;
	private String message;

	public DateResponse(String givenDate, int year, int month, int day, Boolean isValidDate, String message) {
		super();
		this.givenDate = givenDate;
		this.year = year;
		this.month = month;
		this.day = day;
		this.isValidDate = isValidDate;
		this.message = message;
	}

	public DateResponse() {
		super();
	}

	public String getGivenDate() {
		return givenDate;
	}

	public void setGivenDate(String givenDate) {
		this.givenDate = givenDate;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public Boolean getIsValidDate() {
		return isValidDate;
	}

	public void setIsValidDate(Boolean isValidDate) {
		this.isValidDate = isValidDate;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
